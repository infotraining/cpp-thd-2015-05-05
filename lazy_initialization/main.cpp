#include <iostream>
#include <mutex>
#include <thread>

using namespace std;

class Service
{
public:
    virtual void run() = 0;
    virtual ~Service() = default;
};

class RealService : public Service
{
    string url_;
    mutex mtx_;
 public:
    RealService(const string& url) : url_{url}
    {
        cout << "Creating a real service..." << endl;
        cout << "Opening a port for service: " << url_ << endl;
    }

    void run() override
    {
        lock_guard<mutex> lk{mtx_};
        cout << "RealService::run()" << endl;
    }
};

class ProxyService : public Service
{
    unique_ptr<Service> real_service_;
    string url_;
    once_flag init_flag_ {};
public:
    ProxyService(const string& url) : url_{url}
    {
        cout << "Creating light proxy for service..." << endl;
    }

    void run() override
    {
//        if (!real_service_)
//            real_service_.reset(new RealService(url_));
        call_once(init_flag_, [&] { real_service_.reset(new RealService(url_));});
        real_service_->run();
    }
};

int main()
{
    RealService real_srv{"http://service.com"};

    cout << "\n\n";

    real_srv.run();

    cout << "\n\n--------------------------\n\n";

    ProxyService proxy{"http://service.com"};

    cout << "\n\n";

    proxy.run();
}

