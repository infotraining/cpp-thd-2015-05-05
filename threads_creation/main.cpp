#include <iostream>
#include <thread>
#include <cassert>
#include <vector>
#include <functional>

using namespace std;

void hello()
{
    string text = "Hello Concurrent World!";

    for(const auto& c : text)
    {
        cout << c << " ";
        //this_thread::sleep_for(chrono::(100));
        this_thread::yield();
        cout.flush();
    }
}

void background_task(int id, unsigned int count, size_t delay)
{
    for(unsigned int i = 0; i < count; ++i)
    {
        cout << "BT#" << id << ": step#: " << i << endl;

        this_thread::sleep_for(chrono::milliseconds(delay));
    }

    cout << "BT#" << id << " is finished..." << endl;
}

class BackgroundTask
{
    const int id_;
    const unsigned int count_;
public:
    BackgroundTask(int id, unsigned int count) : id_{id}, count_{count}
    {}

    void operator()(unsigned int delay)
    {
        background_task(id_, count_, delay);
    }
};

class Buffer
{
    vector<int> buffer_;
public:
    void assign(const vector<int>& source)
    {
        buffer_.assign(source.begin(), source.end());
    }

    const vector<int>& data() const
    {
        return buffer_;
    }
};

namespace Before
{

class Moveable
{
    size_t size_;
    char* buffer_;

public:
    Moveable(size_t size) : size_{size}, buffer_{new char[size_]}
    {}

    ~Moveable() { delete [] buffer_; }

    Moveable(const Moveable&) = delete;
    Moveable& operator=(const Moveable&) = delete;

    Moveable(Moveable&& source) noexcept : size_{source.size_}, buffer_{source.buffer_}
    {
        source.buffer_ = nullptr;
        source.size_ = 0;
    }

    Moveable& operator=(Moveable&& source) noexcept
    {
        if (this != &source)
        {
            delete [] buffer_;
            buffer_ = source.buffer_;
            size_ = source.size_;

            source.buffer_ = nullptr;
            source.size_ = 0;
        }

        return *this;
    }
};

}

namespace After
{
    class Moveable
    {
        vector<char> buffer_;

    public:
        Moveable(size_t size) : buffer_(size)
        {}

        Moveable(const Moveable&) = default;
        Moveable& operator=(const Moveable&) = default;
        Moveable(Moveable&&) = default;
        Moveable& operator=(Moveable&&) = default;

        virtual ~Moveable() = default;
    };
}

using namespace After;

Moveable create_moveable()
{
    Moveable local_buffer{255};

    return local_buffer;
}


thread start_deamon()
{
    thread thd_background_task(BackgroundTask(665, 40), 500);

    thread thd_moved = move(thd_background_task);

    return thd_moved;
}

int main()
{
    thread deamon = start_deamon();
    deamon.detach();

    thread thd_0;

    assert(!thd_0.joinable());

    cout << thd_0.get_id() << endl;

    thread thd_1(&hello);
    thread thd_2(&hello);

    thd_1.join();
    thd_2.join();

    assert(!thd_1.joinable());
    assert(!thd_2.joinable());

    thread thd_3(&background_task, 3, 10, 100);
    thread thd_4(&background_task, 4, 20, 200);

    thd_3.join();
    thd_4.join();

    // ----------------
    vector<int> data = { 1, 5, 6, 74, 23, 654, 66, 667 };

    Buffer buff1, buff2;

    thread thd_5(bind(&Buffer::assign, ref(buff1), cref(data)));
    thread thd_6([&buff2, &data] { buff2.assign(data);});

    thd_5.join();
    thd_6.join();

    cout << "buff1: ";
    for(const auto& item : buff1.data())
        cout << item << " ";
    cout << endl;

    cout << "buff2: ";
    for(const auto& item : buff2.data())
        cout << item << " ";
    cout << endl;
}












