#include <iostream>
#include <thread>
#include <mutex>

class BankAccount
{
    using KMutex = std::recursive_mutex;

    const int id_;
    double balance_;
    mutable KMutex mtx_;
public:
    BankAccount(int id, double balance) : id_(id), balance_(balance)
    {
    }

    void print() const
    {
        std::cout << "Bank Account #" << id_ << "; Balance = ";
        double current_balance;

        {
            std::lock_guard<KMutex> lk{mtx_};
            current_balance = balance_;
        }

        std::cout << current_balance << std::endl;
    }

    void transfer(BankAccount& to, double amount)
    {
        std::unique_lock<KMutex> lk_from(mtx_, std::defer_lock);
        std::unique_lock<KMutex> lk_to(to.mtx_, std::defer_lock);

        std::lock(lk_from, lk_to);

        withdraw(amount);
        to.deposit(amount);
    }

    void withdraw(double amount)
    {
        std::lock_guard<KMutex> lk{mtx_};

        balance_ -= amount;
    }

    void deposit(double amount)
    {
        std::lock_guard<KMutex> lk{mtx_};

        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        std::lock_guard<KMutex> lk{mtx_};

        return balance_;
    }

    void lock()
    {
        mtx_.lock();
    }

    void unlock()
    {
        mtx_.unlock();
    }
};

void make_withdraws(BankAccount& ba, int no_of_operations)
{
    for(int i = 0; i < no_of_operations; ++i)
        ba.withdraw(1.0);
}

void make_deposits(BankAccount& ba, int no_of_operations)
{
    for(int i = 0; i < no_of_operations; ++i)
        ba.deposit(1.0);
}

void make_transfers(BankAccount& from, BankAccount& to, int no_of_operations, int thd_id)
{
    for(int i = 0; i < no_of_operations; ++i)
    {
        std::cout << "THD#" << thd_id << " transfer from ba#" << from.id()
                  << " to ba#" << to.id() << std::endl;

        from.transfer(to, 1.0);
    }
}

int main()
{
    const int NO_OF_ITERS = 100000;

    BankAccount ba1(1, 10000);
    BankAccount ba2(2, 10000);

    std::thread thd1(&make_withdraws, std::ref(ba1), NO_OF_ITERS);

    {
        std::lock_guard<BankAccount> lk{ba1};

        ba1.withdraw(100);
        ba1.withdraw(100);
        ba1.deposit(400);
        ba1.withdraw(30);
    }

    std::thread thd2(&make_deposits, std::ref(ba1), NO_OF_ITERS);

    thd1.join();
    thd2.join();

    std::cout << "After threads: ";
    ba1.print();

    std::cout << "\nTransfer:" << std::endl;

    std::thread thd3(&make_transfers, std::ref(ba1), std::ref(ba2), NO_OF_ITERS, 1);
    std::thread thd4(&make_transfers, std::ref(ba2), std::ref(ba1), NO_OF_ITERS, 2);

    thd3.join();
    thd4.join();
}
