#include <iostream>
#include <thread>
#include <vector>
#include <list>
#include <algorithm>
#include <chrono>
#include <future>

template <typename It, typename T>
T parallel_accumulate(It first, It last, T init)
{
    unsigned int hardware_thread_count = std::max(std::thread::hardware_concurrency(), 1u);

    std::vector<std::future<T>> partial_results(hardware_thread_count - 1);

    size_t count = std::distance(first, last);
    size_t range_size = count / hardware_thread_count;

    It range_start = first;

    for(unsigned int i = 0; i < hardware_thread_count - 1; ++i)
    {
        It range_end = range_start;
        std::advance(range_end, range_size);

        partial_results[i]
                = std::async(std::launch::async,
                             [=] { return std::accumulate(range_start, range_end, T());});

        range_start = range_end;
    }

    init += std::accumulate(range_start, last, T());

    return std::accumulate(partial_results.begin(), partial_results.end(), init,
                           [](const T& v, std::future<T>& fv) { return v + fv.get();});
}


int main(int argc, char *argv[])
{
    const size_t SIZE = 50000000;

    typedef unsigned long long int ValueType;

    std::vector<ValueType> v(SIZE);
    for (size_t i = 0 ; i < SIZE; ++i)
    {
        v.push_back(i);
    }

    {
        auto start = std::chrono::high_resolution_clock::now();
        std::cout << "Accumulate:          " <<
                     std::accumulate(v.begin(), v.end(), (ValueType)0);
        auto end = std::chrono::high_resolution_clock::now();
        float seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
        std::cout << "   " << seconds << " ms" << std::endl;
    }

    {
        std::cout << "\nParallel\n";

        auto start = std::chrono::high_resolution_clock::now();
        std::cout << "Accumulate:          " <<
                     parallel_accumulate(v.begin(), v.end(), (ValueType)0);
        auto end = std::chrono::high_resolution_clock::now();
        float seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
        std::cout << "   " << seconds << " ms" << std::endl;
    }
}
