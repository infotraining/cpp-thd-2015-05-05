#include <iostream>
#include <mutex>
#include <thread>
#include <future>
#include <vector>

using namespace std;

using FileContent = string;

FileContent download_file(const string& filename)
{
    cout << "Start reading a file: " << filename << endl;

    this_thread::sleep_for(chrono::seconds(2));

    return "Content of file: " + filename;
}

shared_future<FileContent> download_file_async(const string& filename)
{
    packaged_task<FileContent()> task([filename] { return download_file(filename);});
    shared_future<FileContent> sf = task.get_future();
    thread thd{move(task)};
    thd.detach();
    return sf;
}

void process_file(shared_future<FileContent> future_content)
{
    cout << "Start THD#" << this_thread::get_id() << endl;
    try
    {
        FileContent content = future_content.get();
        cout << "Processing: " << content << endl;
    }
    catch(const exception& e)
    {
        cout << "Exception caught: " << e.what() << endl;
    }
}

int main()
{
    vector<shared_future<FileContent>> file_contents
            = { download_file_async("FILE1.txt"), download_file_async("FILE2.txt") };

    file_contents.push_back(download_file_async("FILE3"));

    thread processing_thd{&process_file, file_contents[0]};

    cout << "File contents: ";
    for(auto& fc : file_contents)
        cout << fc.get() << endl;

    processing_thd.join();
}

