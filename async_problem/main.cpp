#include <iostream>
#include <mutex>
#include <thread>
#include <future>
#include <boost/thread/future.hpp>

using namespace std;

void download_file(const string& filename)
{
    cout << "Start reading a file: " << filename << endl;

    this_thread::sleep_for(chrono::seconds(2));

    cout << "Content of file: " + filename << endl;
}

template <typename Callable>
auto spawn_async(Callable&& callable) //-> future<decltype(callable())>
{
    using ResultType = decltype(callable());

    packaged_task<ResultType()> task(move(callable));
    auto future_result = task.get_future();

    thread thd{move(task)};
    thd.detach();

    return future_result;
}

int main()
{
    cout << "std:" << endl;

    async(launch::async, []{ download_file("file1.txt"); });
    async(launch::async, []{ download_file("file2.txt"); });

    cout << "\n\nboost:" << endl;

    boost::async(boost::launch::async, []{ download_file("file1.txt"); });
    boost::async(boost::launch::async, []{ download_file("file2.txt"); });

    cout << "\n\ncustom:";

    spawn_async([]{ download_file("file1.txt"); });
    spawn_async([]{ download_file("file2.txt"); });

    this_thread::sleep_for(chrono::milliseconds(3000));
}

