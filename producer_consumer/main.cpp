#include <iostream>
#include <mutex>
#include <thread>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <boost/thread/sync_queue.hpp>

using namespace std;

using DataType = string;

class ProducerConsumer
{
        ThreadSafeQueue<DataType> queue_;
    static const DataType end_of_work; // poison pill
public:
    void produce(const DataType& data, int consumer_count)
    {
        DataType item = data;

        while (next_permutation(item.begin(), item.end()))
        {
            cout << "Producing: " << item << endl;
            queue_.push(item);
            this_thread::sleep_for(chrono::milliseconds(100));
        }

        // end of job
        for(int i = 0; i < consumer_count; ++i)
            queue_.push(end_of_work);
    }

    void consumer(int id)
    {
        while(true)
        {
            DataType item;
            queue_.wait_and_pop(item);

            if (item != end_of_work)
            {
                cout << "Consumer#" << id << " processed item: " <<
                        process(item) << endl;
            }
            else
            {
                cout << "End of job - consumer#" << id <<  endl;
                break;
            }
        }
    }

private:
    DataType process(const DataType& item)
    {
        this_thread::sleep_for(chrono::milliseconds(500));
        return boost::to_upper_copy(item);
    }
};

const DataType ProducerConsumer::end_of_work = "";

int main()
{
    ProducerConsumer pc;

    thread thd1([&pc] { pc.produce("abcd", 2);});
    thread thd2([&pc] { pc.consumer(1);});
    thread thd3([&pc] { pc.consumer(2);});

    thd1.join();
    thd2.join();
    thd3.join();
}

