#include <iostream>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>

using namespace std;

void background_task(int id, unsigned int count, size_t delay)
{
    for(unsigned int i = 0; i < count; ++i)
    {
        cout << "BT#" << id << ": step#: " << i << endl;

        boost::this_thread::sleep_for(boost::chrono::milliseconds(delay));
    }

    cout << "BT#" << id << " is finished..." << endl;
}

int main()
{
    boost::thread thd(&background_task, 1, 20, 200);

    boost::thread_group threads;

    threads.add_thread(new boost::thread(&background_task, 2, 10, 200));
    threads.create_thread(boost::bind(&background_task, 2, 20, 100));

    threads.join_all();
}

