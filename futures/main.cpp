#include <iostream>
#include <mutex>
#include <thread>
#include <future>
#include <random>

using namespace std;

long calculate_square(int x)
{
    cout << "Starting calculation for " << x << endl;
    if (x == 13)
        throw std::logic_error("Error#13");

    return x * x;
}

class SquareCalculator
{
    promise<long> promise_;
    random_device rd{};
    mt19937_64 gen;
    uniform_int_distribution<> distr;
public:
    SquareCalculator() : gen(rd()), distr(1000, 5000)
    {
    }

    void operator()(int x)
    {
        this_thread::sleep_for(chrono::milliseconds(distr(gen)));

        try
        {
            promise_.set_value(calculate_square(x));
        }
        catch(...)
        {
            auto ex = current_exception();
            promise_.set_exception(ex);
            return;
        }
    }


    future<long> get_future()
    {
        return promise_.get_future();
    }
};

int main()
{
    // 1-szy sposób
    SquareCalculator square_calc;

    future<long> future_square = square_calc.get_future();

    //thread thd1{ref(square_calc), 29 };
    thread thd1{[&square_calc] { square_calc(29); }};
    thd1.detach();

    try
    {
        auto result = future_square.get();
        cout << "29 * 29 = " << result << endl;
    }
    catch(const exception& e)
    {
        cout << "Exception caught: " << e.what() << endl;
    }

    // 2-gi sposób
    packaged_task<long()> pt1([] { return calculate_square(673);});
    packaged_task<long()> pt2(bind(&calculate_square, 665));
    packaged_task<long(int)> pt3(&calculate_square);

    future<long> f1 = pt1.get_future();
    auto f2 = pt2.get_future();
    auto f3 = pt3.get_future();

    thread thd2 {move(pt1)};
    pt2();
    thread thd3 {move(pt3), 32};

    cout << "f2 = " << f2.get() << endl;

    pt2.reset();

    f2 = pt2.get_future();
    pt2();

    try
    {
        cout << "f1 = " << f1.get() << endl;
        cout << "f2 = " << f2.get() << endl;
        cout << "f3 = " << f3.get() << endl;
    }
    catch(const exception& e)
    {
        cout << "Caught exception: " << e.what() << endl;
    }

    thd2.join();
    thd3.join();

    // 3-ci sposób

    cout << "\n\n";

    int x = 55;

    future<long> fs1 = async(launch::async, [x]{ return calculate_square(x); });
    future<long> fs2 = async(&calculate_square, 78);
    future<long> fs3 = async(launch::deferred, &calculate_square, 13);

    vector<future<long>> future_squares;
    future_squares.push_back(move(fs1));
    future_squares.push_back(move(fs2));
    future_squares.push_back(move(fs3));

    cout << "Results: " << endl;
    for(auto& fs : future_squares)
    {
        try
        {
            cout << fs.get() << endl;
        }
        catch(const exception& e)
        {
            cout << "Caught exception: " << e.what() << endl;
        }
    }

}

