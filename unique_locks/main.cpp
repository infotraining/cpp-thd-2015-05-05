#include <iostream>
#include <mutex>
#include <thread>
#include <chrono>

using namespace std;

timed_mutex mtx;


void background_task(int id, int timeout)
{
    cout << "BT#" << id << " is waiting for a mutex..." << endl;

    unique_lock<timed_mutex> lk(mtx, try_to_lock);

    if (!lk.owns_lock())
    {
        do
        {
            cout << "BT#" << id << " doesn't own lock..."
                 << " Tries to acquire a mutex..." << endl;
        } while(!lk.try_lock_for(chrono::milliseconds(timeout)));
    }

    cout << "Start of BT#" << id << endl;

    this_thread::sleep_for(chrono::seconds(10));

    cout << "End of BT#" << id << endl;
}

int main()
{
    thread thd1(&background_task, 1, 500);
    thread thd2(&background_task, 2, 750);

    thd1.join();
    thd2.join();
}

