#include <iostream>
#include <mutex>
#include <thread>
#include <algorithm>
#include <random>
#include <atomic>
#include <condition_variable>

using namespace std;

namespace atomic_impl
{

class Data
{
    vector<int> data_;
    atomic<bool> is_ready_ {};

public:
    void read()
    {
        cout << "Start reading..." << endl;
        this_thread::sleep_for(chrono::milliseconds(2000));

        data_.resize(100);

        random_device rd;
        mt19937_64 gen(rd());
        uniform_int_distribution<> dist(1, 100);

        generate(data_.begin(), data_.end(), [&dist, &gen] {return dist(gen);});

        is_ready_ = true;
    }

    void process(int id)
    {
        while (!is_ready_);

        long sum = accumulate(data_.begin(), data_.end(), 0L);

        cout << "Id: " << id << " Sum: " << sum << endl;
    }
};

void use_data()
{
    Data data;

    thread thd1([&data] { data.read();});

    thread thd2([&data] { data.process(1);});
    thread thd3([&data] { data.process(2);});

    thd1.join();
    thd2.join();
    thd3.join();
}

class Singleton
{
    static atomic<Singleton*> unique_instance_;
    static mutex mtx_;

    static Singleton& instance()
    {
        if (!unique_instance_.load())
        {
            lock_guard<mutex> lk(mtx_);

            if (!unique_instance_.load())
            {
                Singleton* temp = new Singleton;
                unique_instance_.store(temp);
            }
        }

        return *unique_instance_;
    }
};

atomic<Singleton*> Singleton::unique_instance_{};

}

namespace condition_variable_impl
{
class Data
{
    vector<int> data_;
    bool is_ready_ = false;
    mutex cv_mtx_;
    condition_variable cv_;

public:
    void read()
    {
        cout << "Start reading..." << endl;
        this_thread::sleep_for(chrono::milliseconds(2000));

        data_.resize(100);

        random_device rd;
        mt19937_64 gen(rd());
        uniform_int_distribution<> dist(1, 100);

        generate(data_.begin(), data_.end(), [&dist, &gen] {return dist(gen);});

        unique_lock<mutex> lk(cv_mtx_);
        is_ready_ = true;
        lk.unlock();
        cv_.notify_all();
    }

    void process(int id)
    {
        unique_lock<mutex> lk(cv_mtx_);

        cv_.wait(lk, [this] { return is_ready_; });

        lk.unlock();

        long sum = accumulate(data_.begin(), data_.end(), 0L);

        cout << "Id: " << id << " Sum: " << sum << endl;
    }
};

void use_data()
{
    Data data;

    thread thd1([&data] { data.read();});

    thread thd2([&data] { data.process(1);});
    thread thd3([&data] { data.process(2);});

    thd1.join();
    thd2.join();
    thd3.join();
}

}

int main()
{
    condition_variable_impl::use_data();
}

