#include <iostream>
#include <mutex>
#include <thread>
#include "thread_safe_queue.hpp"
#include <memory>

using namespace std;

using Task = function<void()>;

class ThreadPool
{
public:
    ThreadPool(size_t no_of_threads)
    {
        for(size_t i = 0; i < no_of_threads; ++i)
            threads_.emplace_back([this] { run(); });
    }

    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    ~ThreadPool()
    {
        for(size_t i = 0; i < threads_.size(); ++i)
            submit(END_OF_WORK);

        for(auto& t : threads_)
            t.join();
    }

    void submit(Task task)
    {
        task_queue_.push(task);
    }
private:
    const static nullptr_t END_OF_WORK;
    ThreadSafeQueue<Task> task_queue_;
    vector<thread> threads_;

    void run()
    {
        while (true)
        {
            Task task;
            task_queue_.wait_and_pop(task);

            if (task == END_OF_WORK)
                return;

            task(); // execution of task
        }
    }
};

const nullptr_t ThreadPool::END_OF_WORK = nullptr;


void background_task(int id)
{
    cout << "BT#" << id << " starts..." << endl;
    this_thread::sleep_for(chrono::milliseconds(rand() % 3000));
    cout << "BT#" << id << " ends..." << endl;
}

int main()
{
    ThreadPool pool(8);

    pool.submit([] { background_task(1);});
    pool.submit([] { background_task(2);});

    for(int i = 3; i < 200; ++i)
        pool.submit([i] { background_task(i);});
}

