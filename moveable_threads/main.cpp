#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void background_task(int id, unsigned int count, size_t delay)
{
    for(unsigned int i = 0; i < count; ++i)
    {
        cout << "BT#" << id << ": step#: " << i << endl;

        this_thread::sleep_for(chrono::milliseconds(delay));
    }

    cout << "BT#" << id << " is finished..." << endl;
}

int main()
{
    thread thd(&background_task, 1, 30, 500);

    vector<thread> threads;

    threads.push_back(thread(&background_task, 2, 20, 300));
    threads.push_back(move(thd));
    threads.emplace_back(&background_task, 3, 20, 100);

    for(auto& t : threads)
        t.join();
}

