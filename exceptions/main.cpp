#include <iostream>
#include <mutex>
#include <thread>
#include <vector>
#include <sstream>

using namespace std;

void may_throw(int arg)
{
    if (arg % 13 == 0)
    {
        cout << "---- THROW ERROR#13 --------" << endl;

        stringstream ss;
        ss << "Error#13 - call Redmond! - thread_id: " << this_thread::get_id();
        string message = ss.str();

        throw runtime_error(message);
    }
}

void background_task(int id, unsigned int count, size_t delay, exception_ptr& excpt)
{
    try
    {
        for(unsigned int i = 1; i <= count; ++i)
        {
            cout << "BT#" << id << ": step#: " << i << endl;

            this_thread::sleep_for(chrono::milliseconds(delay));

            may_throw(i);
        }

        cout << "BT#" << id << " is finished..." << endl;
    }
    catch(...)
    {
        cout << "Caught exception in thead: " << this_thread::get_id() << endl;
        excpt = current_exception();
    }
}

int main()
try
{
    cout << "Hardware concurrency: " << thread::hardware_concurrency() << endl;

    const int N = 4;

    vector<exception_ptr> thread_exceptions(N);
    vector<thread> threads(N);

    for(int i = 0; i < N; ++i)
        threads[i] = thread(&background_task, i + 1, 15, 100, ref(thread_exceptions[i]));

    for(auto& t : threads)
        t.join();

    // exception handling expct1
    for(auto& e : thread_exceptions)
    {
        if (e)
        {
            try
            {
                rethrow_exception(e);
            }
            catch(const exception& excpt)
            {
                cout << "Main catch: " << excpt.what() << endl;
            }
        }
    }
}
catch(const exception& e)
{
    cout << "Cought: " << e.what() << endl;
}

