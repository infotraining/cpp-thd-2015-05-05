#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

using namespace std;

void background_task(int id, unsigned int count, size_t delay)
{
    for(unsigned int i = 0; i < count; ++i)
    {
        cout << "BT#" << id << ": step#: " << i << endl;

        this_thread::sleep_for(chrono::milliseconds(delay));
    }

    cout << "BT#" << id << " is finished..." << endl;
}

void may_throw(int arg)
{
    if (arg % 13 == 0)
    {
        cout << "---- THROW ERROR#13 --------" << endl;
        throw runtime_error("Error#13 - call Redmond!");
    }
}

enum class DtorAction
{
    join, detach
};

class ThreadGuard
{
public:
    ThreadGuard(DtorAction action, thread&& thd)
        : action_{action}, thd_{move(thd)}
    {}

    template <typename... Args>
    ThreadGuard(DtorAction action, Args&&... args)
        : action_{action}, thd_{forward<Args>(args)...}
    {}

    //ThreadGuard(DtorAction:join, &background_work, 1, 20, 200)

    ThreadGuard(const ThreadGuard&) = delete;
    ThreadGuard& operator=(const ThreadGuard&) = delete;

    ThreadGuard(ThreadGuard&&) = default;
    ThreadGuard& operator=(ThreadGuard&&) = default;

    ~ThreadGuard()
    {
        if (thd_.joinable())
        {
            if (action_ == DtorAction::join)
                thd_.join();
            else
                thd_.detach();
        }
    }

    thread& get()
    {
        return thd_;
    }

private:
    DtorAction action_;
    thread thd_;
};

void do_work()
{
    ThreadGuard thd{DtorAction::join, &background_task, 1, 20, 200};

    vector<ThreadGuard> threads;

    threads.push_back(move(thd));
    threads.emplace_back(DtorAction::join, &background_task, 2, 20, 400);
    threads.emplace_back(DtorAction::detach, &background_task, 3, 10, 1000);

    for(int i = 1; i <= 20; ++i)
    {
        cout << "do_work(" << i << ")...." << endl;
        this_thread::sleep_for(chrono::milliseconds(200));
        may_throw(i);
    }
}

int main() try
{
    do_work();
}
catch(const exception& e)
{
    cout << "Cought: " << e.what() << endl;
}











