#include <iostream>
#include <random>
#include <chrono>
#include <vector>
#include <thread>
#include <atomic>
#include <mutex>
#include <future>

using namespace std;

long calc_hits(long N)
{
    long counter = 0;

    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);

    for (long n = 0 ; n < N ; ++n)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y <= 1)
            ++counter;
    }

    return counter;
}

void single_threaded_pi(long N)
{
    auto start = chrono::high_resolution_clock::now();

    double mc_pi = static_cast<double>(calc_hits(N))/N * 4.0;

    auto end = chrono::high_resolution_clock::now();

    cout << "Pi = " << mc_pi << endl;
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;
}


namespace Lambdas
{
    class Lambda_23423423
    {
        const int a;
        int& b;
    public:
        Lambda_23423423(int a, int& b) : a(a), b(b)
        {}

        void operator()(int param) const
        {
            cout << a << (b++) << param;
        }
    };

    int a = 10;
    int b = 20;

    auto lambda = [](int param) mutable { cout << a << (b++) << param; };
}

void multithreaded_pi(long N)
{
    vector<thread> threads;

    unsigned int no_of_threads = max(thread::hardware_concurrency(), 1u);
    vector<long> partial_results(no_of_threads);

    auto start = chrono::high_resolution_clock::now();

    long N_per_thread = N / no_of_threads;

    for(unsigned int i = 0; i < no_of_threads; ++i)
        threads.emplace_back([i, &partial_results, N_per_thread] {
                                partial_results[i] = calc_hits(N_per_thread);
                             });
    for(auto& th : threads)
        th.join();

    auto total_counts = accumulate(partial_results.begin(), partial_results.end(), 0L);

    auto mc_pi = static_cast<double>(total_counts)/N * 4.0;

    auto end = chrono::high_resolution_clock::now();

    cout << "Pi = " << mc_pi << endl;
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;
}

void multithreaded_pi_with_async(long N)
{
    unsigned int no_of_threads = max(thread::hardware_concurrency(), 1u);
    vector<future<long>> partial_results(no_of_threads);

    auto start = chrono::high_resolution_clock::now();

    long N_per_thread = N / no_of_threads;

    for(unsigned int i = 0; i < no_of_threads; ++i)
        partial_results[i] = async(launch::async,
                                   [N_per_thread] {return calc_hits(N_per_thread);});

    auto total_counts = accumulate(partial_results.begin(), partial_results.end(), 0L,
                                   [](long v, future<long>& fv) { return v + fv.get(); });

    auto mc_pi = static_cast<double>(total_counts)/N * 4.0;

    auto end = chrono::high_resolution_clock::now();

    cout << "Pi = " << mc_pi << endl;
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;
}

long shared_counter = 0;
mutex mtx_counter_;

void lock_shared_counter_mutex()
{
    mtx_counter_.lock();
}

void count_hits_with_shared_counter(long N)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);

    for (long n = 0 ; n < N ; ++n)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y <= 1)
        {
            lock_guard<mutex> lk{mtx_counter_};
            ++shared_counter;
        }
    }
}

void multithreaded_pi_with_mutexes(long N)
{
    vector<thread> threads;

    unsigned int no_of_threads = max(thread::hardware_concurrency(), 1u);

    long N_per_thread = N / no_of_threads;

    auto start = chrono::high_resolution_clock::now();

    for(unsigned int i = 0; i < no_of_threads; ++i)
    {
        threads.emplace_back(&count_hits_with_shared_counter, N_per_thread);
    }

    for(auto& thd : threads)
        thd.join();

    auto mc_pi = static_cast<double>(shared_counter)/N * 4.0;

    auto end = chrono::high_resolution_clock::now();

    cout << "Pi = " << mc_pi << endl;
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;
}

atomic<long> atomic_counter {};

void count_hits_with_shared_atomic_counter(long N)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);

    for (long n = 0 ; n < N ; ++n)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y <= 1)
        {
            ++atomic_counter;
        }
    }
}

void multithreaded_pi_with_atomics(long N)
{
    vector<thread> threads;

    unsigned int no_of_threads = max(thread::hardware_concurrency(), 1u);

    long N_per_thread = N / no_of_threads;

    auto start = chrono::high_resolution_clock::now();

    for(unsigned int i = 0; i < no_of_threads; ++i)
    {
        threads.emplace_back(&count_hits_with_shared_atomic_counter, N_per_thread);
    }

    for(auto& thd : threads)
        thd.join();

    auto mc_pi = static_cast<double>(shared_counter)/N * 4.0;

    auto end = chrono::high_resolution_clock::now();

    cout << "Pi = " << mc_pi << endl;
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;
}


int main()
{
    long N = 10000000;

    cout << "single_threaded_pi(N)" << endl;

    single_threaded_pi(N);

    cout << "\n\nmultithreaded_pi(N)" << endl;

    multithreaded_pi(N);

    cout << "\n\nmultithreaded_pi_with_async(N)" << endl;

    multithreaded_pi_with_async(N);

    cout << "\n\nmultithreaded_pi_with_mutexes(N)" << endl;

    multithreaded_pi_with_mutexes(N);

    cout << "\n\nmultithreaded_pi_with_atomics(N)" << endl;

    multithreaded_pi_with_atomics(N);
}
